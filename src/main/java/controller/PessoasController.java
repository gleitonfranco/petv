package controller;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import dao.PessoaDao;
import model.Pessoa;

import java.util.List;

@Resource
public class PessoasController {

    private PessoaDao dao;
    private Result result;

    public PessoasController(PessoaDao dao, Result result) {
        this.dao = dao;
        this.result = result;
    }

//  public void list() {
    public List<Pessoa> list() {
//        this.result.include("list", dao.obterTodos());
        return this.dao.obterTodos();
    }
}
