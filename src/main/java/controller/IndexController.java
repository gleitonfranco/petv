package controller;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class IndexController {
    Result result;

    public IndexController(Result result) {
        this.result = result;
    }

    public void hello() {
        this.result.include("mensagem","Mensagem de Index");
    }

}
