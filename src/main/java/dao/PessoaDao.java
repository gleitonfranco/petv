package dao;

import br.com.caelum.vraptor.Result;
import model.Pessoa;

import java.util.ArrayList;
import java.util.List;

public class PessoaDao {
    private Result result;
    private List<Pessoa> pessoas;

    public PessoaDao() {
        this.pessoas = new ArrayList<>();
        this.pessoas.add(new Pessoa(1L, "Gleiton Soares Franco"));
        this.pessoas.add(new Pessoa(2L, "Margarida Soares Franco"));
        this.pessoas.add(new Pessoa(3L, "Vanessa Soares Franco"));
        this.pessoas.add(new Pessoa(4L, "Beatriz Soares Franco"));
        this.pessoas.add(new Pessoa(5L, "Milton Soares Franco"));
        this.pessoas.add(new Pessoa(6L, "Gleicer Soares Franco"));
        this.pessoas.add(new Pessoa(7L, "Audrey Soares Franco"));
        this.pessoas.add(new Pessoa(8L, "Luiz Soares Franco"));
        this.pessoas.add(new Pessoa(9L, "Guilherme Soares Franco"));
    }

    public List<Pessoa> obterTodos() {
        return this.pessoas;
    }
}
