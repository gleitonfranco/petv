<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Pessoa" %>
<html>
<body>
<h1>Lista de Pessoas</h1>

<table>
    <thead><tr><th>ID</th><th>NOME</th></tr></thead>
    <tbody>
        <c:forEach items="${pessoaList}" var="pessoa">
            <tr>
                <td>${pessoa.id}</td>
                <td>${pessoa.nome}</td>
                <td><a href="#">Editar</a> </td>
                <td><a href="#">Remover</a> </td>
            </tr>
        </c:forEach>
    </tbody>
</table>

</body>
</html>
